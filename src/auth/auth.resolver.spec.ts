import { Test, TestingModule } from '@nestjs/testing';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';
import { LoginUserInput } from './dto/login-user.input';

const mockAuthService = {
  login: jest.fn(),
  signup: jest.fn(),
};

describe('AuthResolver', () => {
  let authResolver: AuthResolver;

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthResolver,
        {
          provide: AuthService,
          useValue: mockAuthService,
        },
      ],
    }).compile();

    authResolver = module.get<AuthResolver>(AuthResolver);
  });

  it('should be defined', () => {
    expect(authResolver).toBeDefined();
  });

  it('should call authService.login', () => {
    const args: LoginUserInput = new LoginUserInput();
    args.username = 'testuser';
    args.password = 'testpassword';

    const context = {
      user: { id: '1', username: 'testuser' },
    };

    mockAuthService.login.mockReturnValue({
      access_token: 'sample_access_token',
      user: context.user,
    });

    const result = authResolver.login(args, context);

    expect(result).toBeDefined();
    expect(mockAuthService.login).toHaveBeenCalledWith(context.user);
  });

  it('should call authService.signup', () => {
    const registerUserInput = {
      username: 'newuser',
      phoneNo: '1234567890',
      password: 'newpassword',
    };

    mockAuthService.signup.mockReturnValue({
      username: registerUserInput.username,
      phoneNo: registerUserInput.phoneNo,
    });

    const result = authResolver.signup(registerUserInput);

    expect(result).toBeDefined();
    expect(mockAuthService.signup).toHaveBeenCalledWith(registerUserInput);
  });
});
