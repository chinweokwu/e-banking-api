import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsersService } from './../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { RegisterUserInput } from './dto/register-user.input';

jest.mock('bcrypt');
const mockUsersService = {
  findOne: jest.fn(),
  create: jest.fn(),
};
const mockJwtService = {
  sign: jest.fn(),
};

describe('AuthService', () => {
  let authService: AuthService;

  beforeEach(async () => {
    jest.clearAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: mockUsersService,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  it('should validate a user', async () => {
    const username = 'testuser';
    const password = 'testpassword';
    const hashedPassword = 'hashedpassword';

    mockUsersService.findOne.mockResolvedValue({
      username,
      password: hashedPassword,
    });

    jest.spyOn(bcrypt, 'compare').mockResolvedValue(true as never);

    const user = await authService.validateUser(username, password);

    expect(user).toBeDefined();
    expect(user.username).toBe(username);
  });

  it('should return null when validating an invalid user', async () => {
    const username = 'testuser';
    const password = 'testpassword';

    mockUsersService.findOne.mockResolvedValue(null);

    const user = await authService.validateUser(username, password);

    expect(user).toBeNull();
  });

  it('should create a new user', async () => {
    const registerUserInput: RegisterUserInput = {
      username: 'newuser',
      phoneNo: '1234567890',
      password: 'newpassword',
    };
    const hashedPassword = 'hashedpassword';

    mockUsersService.findOne.mockResolvedValue(null);
    jest.spyOn(bcrypt, 'hash').mockImplementation(async () => {
      return Promise.resolve(hashedPassword);
    });
    mockUsersService.create.mockResolvedValue({
      ...registerUserInput,
      password: hashedPassword,
    });

    const newUser = await authService.signup(registerUserInput);

    expect(newUser).toBeDefined();
    expect(newUser.username).toBe(registerUserInput.username);
  });

  it('should throw an error when trying to create a user with an existing username', async () => {
    const registerUserInput: RegisterUserInput = {
      username: 'existinguser',
      phoneNo: '1234567890',
      password: 'newpassword',
    };

    mockUsersService.findOne.mockResolvedValue({});

    try {
      await authService.signup(registerUserInput);
    } catch (error) {
      expect(error.message).toBe('Error during signup: User already exists');
    }
  });
});
