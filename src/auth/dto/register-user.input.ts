import { InputType, Field } from '@nestjs/graphql';
import { Prop } from '@nestjs/mongoose';

@InputType()
export class RegisterUserInput {
  @Field()
  @Prop({ required: true, unique: true, minlength: 3, maxlength: 30 })
  username: string;

  @Field()
  @Prop({ required: true, unique: true, minlength: 11, maxlength: 11 })
  phoneNo: string;

  @Field()
  @Prop({ required: true })
  password: string;
}
