import { ConflictException, Injectable } from '@nestjs/common';
import { UsersService } from './../users/users.service';
import { User } from './../users/entities/user.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { RegisterUserInput } from './dto/register-user.input';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    try {
      const user = await this.usersService.findOne(username);

      if (user) {
        const isValid = await bcrypt.compare(password, user.password);

        if (isValid) {
          return user;
        }
      }

      return null;
    } catch (error) {
      throw new Error('Error validating user: ' + error.message);
    }
  }

  async login(user: User) {
    const payload = {
      sub: user._id,
      username: user.username,
      balance: user.balance,
    };

    return {
      access_token: this.jwtService.sign(payload),
      user,
    };
  }

  async signup(registerUserInput: RegisterUserInput) {
    try {
      const user = await this.usersService.findOne(registerUserInput.username);

      if (user) {
        throw new ConflictException('User already exists');
      }

      const password = await bcrypt.hash(registerUserInput.password, 10);

      const newUser = await this.usersService.create({
        ...registerUserInput,
        password,
      });

      return newUser;
    } catch (error) {
      throw new Error('Error during signup: ' + error.message);
    }
  }
}
