import { Test, TestingModule } from '@nestjs/testing';
import { TransactionService } from './transaction.service';
import { CreateTransactionInput } from './dto/create-transaction.input';
import { Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';

describe('TransactionService', () => {
  let service: TransactionService;

  const mockTransactionData: any[] = [
    {
      _id: '1',
      userId: Object('1234567876543'),
      amount: 100,
      type: 'deposit',
      timestamp: new Date(),
    },
    {
      _id: '2',
      userId: Object('1234567876543'),
      amount: 200,
      type: 'withdrawal',
      timestamp: new Date(),
    },
  ];

  const mockTransactionModel = {
    create: jest.fn((input) => {
      const newTransaction = {
        _id: (mockTransactionData.length + 1).toString(),
        ...input,
      };
      mockTransactionData.push(newTransaction);
      return Promise.resolve(newTransaction);
    }),
    find: jest.fn(() => ({
      sort: jest.fn(() => Promise.resolve(mockTransactionData)),
    })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionService,
        {
          provide: getModelToken('Transaction'),
          useValue: mockTransactionModel as unknown as Model<any>,
        },
      ],
    }).compile();

    service = module.get<TransactionService>(TransactionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should throw an error if transaction creation fails', async () => {
      const createTransactionInput: CreateTransactionInput = {
        userId: Object('1234567876543'),
        amount: 100,
        type: 'deposit',
        timestamp: new Date(),
        recipientPhoneNo: ''
      };

      const errorMessage = 'Failed to create transaction';
      mockTransactionModel.create.mockRejectedValue(new Error(errorMessage));

      await expect(service.create(createTransactionInput)).rejects.toThrowError(
        errorMessage,
      );
    });
  });

  describe('findTransactionsByUserId', () => {
    it('should find transactions by user ID', async () => {
      const userId = Object('1234567876543');

      const result = await service.findTransactionsByUserId(userId);

      expect(result).toEqual(mockTransactionData);
    });
  });
});
