/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { TransactionResolver } from './transaction.resolver';
import { TransactionService } from './transaction.service';
import { UsersService } from '../users/users.service';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Transaction } from './entities/transaction.entity';
import { CreateTransactionInput } from './dto/create-transaction.input';

const mockTransaction = {
  _id: '1',
  userId: Object('1234567876543'),
  amount: 100,
};

const mockTransactionService = {
  create: jest.fn().mockResolvedValue(mockTransaction),
  findTransactionsByUserId: jest.fn().mockResolvedValue([mockTransaction]),
};

const mockUsersService = {
  findById: jest
    .fn()
    .mockResolvedValue({ _id: Object('1234567876543'), balance: 200 }),
  updateBalance: jest.fn(),
  findByPhoneNo: jest.fn().mockResolvedValue({
    _id: Object('987654321'),
    balance: 100,
    phoneNo: 'recipient-phone-no',
  }),
};

describe('TransactionResolver', () => {
  let resolver: TransactionResolver;
  let transactionModel: Model<Transaction>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionResolver,
        {
          provide: TransactionService,
          useValue: mockTransactionService,
        },
        {
          provide: UsersService,
          useValue: mockUsersService,
        },
        {
          provide: getModelToken(Transaction.name),
          useValue: {
            create: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<TransactionResolver>(TransactionResolver);
    transactionModel = module.get<Model<Transaction>>(
      getModelToken(Transaction.name),
    );
  });

  describe('deposit', () => {
    it('should deposite transaction and update balance', async () => {
      const amount = 100;
      const user = { userId: Object('1234567876543') };

      const result = await resolver.deposit(amount, user);

      expect(mockUsersService.findById).toHaveBeenCalledWith(user.userId);
      expect(mockUsersService.updateBalance).toHaveBeenCalledWith(
        user.userId,
        300,
      );

      const expectedTransaction: CreateTransactionInput = {
        userId: Object('1234567876543'),
        amount,
        type: 'deposit',
        timestamp: expect.any(Date),
        recipientPhoneNo: '',
      };

      expect(mockTransactionService.create).toHaveBeenCalledWith(
        expectedTransaction,
      );

      expect(result).toEqual(mockTransaction);
    });

    it('should throw an error if the amount is not greater than zero', async () => {
      const amount = 0;
      const user = { userId: Object('1234567876543') };

      await expect(resolver.deposit(amount, user)).rejects.toThrowError(
        'Amount must be greater than zero',
      );
    });
  });

  describe('withdraw', () => {
    it('should create withdrawal transaction and update balance', async () => {
      const amount = 50;
      const user = { userId: Object('1234567876543') };
      const mockUser = { _id: Object('1234567876543'), balance: 200 };

      mockUsersService.findById.mockResolvedValue(mockUser);

      const result = await resolver.withdraw(amount, user);

      expect(mockUsersService.findById).toHaveBeenCalledWith(user.userId);
      expect(mockUsersService.updateBalance).toHaveBeenCalledWith(
        user.userId,
        150,
      );

      const expectedTransaction: CreateTransactionInput = {
        userId: Object('1234567876543'),
        amount,
        type: 'withdrawal',
        timestamp: expect.any(Date),
        recipientPhoneNo: '',
      };

      expect(mockTransactionService.create).toHaveBeenCalledWith(
        expectedTransaction,
      );

      expect(result).toEqual(mockTransaction);
    });

    it('should throw an error if the amount is not greater than zero', async () => {
      const amount = 0;
      const user = { userId: Object('1234567876543') };

      await expect(resolver.withdraw(amount, user)).rejects.toThrowError(
        'Amount must be greater than zero',
      );
    });

    it('should throw an error if the amount exceeds the user balance', async () => {
      const amount = 300;
      const user = { userId: Object('1234567876543') };
      const mockUser = { _id: Object('1234567876543'), balance: 200 };

      mockUsersService.findById.mockResolvedValue(mockUser);

      await expect(resolver.withdraw(amount, user)).rejects.toThrowError(
        'Insufficient balance',
      );
    });
  });

  describe('transactionHistory', () => {
    it('should fetch transaction history for the user', async () => {
      const user = { userId: Object('1234567876543') };
      const mockTransactions = [mockTransaction];

      mockTransactionService.findTransactionsByUserId.mockResolvedValue(
        mockTransactions,
      );

      const result = await resolver.transactionHistory(user);

      expect(
        mockTransactionService.findTransactionsByUserId,
      ).toHaveBeenCalledWith(user.userId);
      expect(result).toEqual(mockTransactions);
    });

    it('should handle errors when fetching transaction history', async () => {
      const user = { userId: Object('1234567876543') };
      const errorMessage = 'Failed to fetch transactions';

      mockTransactionService.findTransactionsByUserId.mockRejectedValue(
        new Error(errorMessage),
      );

      await expect(resolver.transactionHistory(user)).rejects.toThrowError(
        errorMessage,
      );
    });
  });

  describe('transfer', () => {
    it('should transfer money and update balances', async () => {
      const user = { userId: Object('1234567876543') };
      const recipientPhoneNo = 'recipient-phone-no';
      const amount = 50;
      const fetchedUser = { _id: Object('1234567876543'), balance: 200 };
      const recipientUser = { _id: Object('987654321'), balance: 100 };

      mockUsersService.findById.mockResolvedValue(fetchedUser);
      mockUsersService.findByPhoneNo.mockResolvedValue(recipientUser);

      const result = await resolver.transfer(amount, recipientPhoneNo, user);

      expect(mockUsersService.findById).toHaveBeenCalledWith(user.userId);
      expect(mockUsersService.findByPhoneNo).toHaveBeenCalledWith(
        recipientPhoneNo,
      );
      expect(mockUsersService.updateBalance).toHaveBeenCalledWith(
        user.userId,
        150,
      );
      expect(mockUsersService.updateBalance).toHaveBeenCalledWith(
        recipientUser._id,
        150,
      );
    });

    it('should throw an error if the amount is not greater than zero', async () => {
      const amount = 0;
      const user = { userId: Object('1234567876543') };
      const recipientPhoneNo = '1234567890';

      await expect(
        resolver.transfer(amount, recipientPhoneNo, user),
      ).rejects.toThrowError('Amount must be greater than zero');
    });

    it('should throw an error if the amount exceeds the user balance', async () => {
      const amount = 300;
      const user = { userId: Object('1234567876543') };
      const recipientPhoneNo = '1234567890';

      await expect(
        resolver.transfer(amount, recipientPhoneNo, user),
      ).rejects.toThrowError('Insufficient balance');
    });

    it('should throw an error if trying to transfer money to oneself', async () => {
      const amount = 50;
      const user = { userId: Object('1234567876543') };
      const recipientPhoneNo = '1234567890';

      mockUsersService.findById.mockResolvedValue({
        _id: Object('987654321'),
        balance: 200,
        phoneNo: '1234567890',
      });

      await expect(
        resolver.transfer(amount, recipientPhoneNo, user),
      ).rejects.toThrowError('Cannot transfer money to yourself');
    });
  });
});
