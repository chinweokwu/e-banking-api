import { Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@ObjectType()
@Schema()
export class Transaction {
  @Field()
  @Prop({ required: true })
  userId: string;

  @Field()
  @Prop({ required: true })
  amount: number;

  @Field()
  @Prop({ required: true })
  type: string;

  @Field(() => String)
  @Prop()
  recipientPhoneNo: string;

  @Field()
  @Prop()
  timestamp: Date;
  _id: any;
}

export type TransactionDocument = Transaction & Document;
export const TransactionSchema = SchemaFactory.createForClass(Transaction);
