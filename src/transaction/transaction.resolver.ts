import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { TransactionService } from './transaction.service';
import { Transaction } from './entities/transaction.entity';
import { CreateTransactionInput } from './dto/create-transaction.input';
import { UsersService } from './../users/users.service';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { CurrentUser } from './../auth/user.decorator';

@Resolver(() => Transaction)
@UseGuards(JwtAuthGuard)
export class TransactionResolver {
  constructor(
    private transactionService: TransactionService,
    private usersService: UsersService,
  ) {}

  @Mutation(() => Transaction)
  @UseGuards(JwtAuthGuard)
  async deposit(
    @Args('amount') amount: number,
    @CurrentUser() user: any,
  ): Promise<Transaction> {
    try {
      const fetchedUser = await this.usersService.findById(user.userId);

      if (isNaN(amount) || amount <= 0) {
        throw new Error('Amount must be greater than zero');
      }

      const updatedBalance = fetchedUser.balance + amount;

      await this.usersService.updateBalance(fetchedUser._id, updatedBalance);

      const createTransactionInput: CreateTransactionInput = {
        userId: fetchedUser._id,
        amount,
        type: 'deposit',
        timestamp: new Date(),
        recipientPhoneNo: ''
      };

      return this.transactionService.create(createTransactionInput);
    } catch (error) {
      throw new Error('Failed to deposit: ' + error.message);
    }
  }

  @Mutation(() => Transaction)
  @UseGuards(JwtAuthGuard)
  async withdraw(
    @Args('amount') amount: number,
    @CurrentUser() user: any,
  ): Promise<Transaction> {
    try {
      const fetchedUser = await this.usersService.findById(user.userId);

      if (isNaN(amount) || amount <= 0) {
        throw new Error('Amount must be greater than zero');
      }

      if (amount > fetchedUser.balance) {
        throw new Error('Insufficient balance');
      }

      const updatedBalance = fetchedUser.balance - amount;

      await this.usersService.updateBalance(fetchedUser._id, updatedBalance);

      const createTransactionInput: CreateTransactionInput = {
        userId: fetchedUser._id,
        amount,
        type: 'withdrawal',
        timestamp: new Date(),
        recipientPhoneNo: ''
      };

      const transaction = await this.transactionService.create(
        createTransactionInput,
      );

      return transaction;
    } catch (error) {
      throw new Error('Failed to withdraw: ' + error.message);
    }
  }


  @Mutation(() => Transaction)
  @UseGuards(JwtAuthGuard)
  async transfer(
    @Args('amount') amount: number,
    @Args('recipientPhoneNo') recipientPhoneNo: string,
    @CurrentUser() user: any,
  ): Promise<Transaction> {
    try {
      const fetchedUser = await this.usersService.findById(user.userId);
  
      if (isNaN(amount) || amount <= 0) {
        throw new Error('Amount must be greater than zero');
      }
  
      if (amount > fetchedUser.balance) {
        throw new Error('Insufficient balance');
      }
  
      const recipientUser = await this.usersService.findByPhoneNo(recipientPhoneNo);
  
      if (!recipientUser) {
        throw new Error('Recipient not found');
      }

      if (fetchedUser._id.toString() === recipientUser._id.toString()) {
        throw new Error('Cannot transfer money to yourself');
      }
  
      // Update sender's balance
      const updatedSenderBalance = fetchedUser.balance - amount;
      await this.usersService.updateBalance(fetchedUser._id, updatedSenderBalance);
  
      // Update recipient's balance
      const updatedRecipientBalance = recipientUser.balance + amount;
      await this.usersService.updateBalance(recipientUser._id, updatedRecipientBalance);
  
      // Create transaction for the sender
      const senderTransaction: CreateTransactionInput = {
        userId: fetchedUser._id,
        amount: amount,
        type: 'transfer_sent',
        timestamp: new Date(),
        recipientPhoneNo: recipientUser.phoneNo
      };
      await this.transactionService.create(senderTransaction);
  
      // Create transaction for the recipient
      const recipientTransaction: CreateTransactionInput = {
        userId: recipientUser._id,
        amount: amount,
        type: 'transfer_received',
        timestamp: new Date(),
        recipientPhoneNo: fetchedUser.phoneNo
      };
      const transaction = await this.transactionService.create(recipientTransaction);
  
      return transaction;
    } catch (error) {
      throw new Error('Failed to transfer money: ' + error.message);
    }
  }

  @Query(() => [Transaction])
  async transactionHistory(@CurrentUser() user: any): Promise<Transaction[]> {
    try {
      return this.transactionService.findTransactionsByUserId(user.userId);
    } catch (error) {
      throw new Error('Failed to fetch transaction history: ' + error.message);
    }
  }
}
