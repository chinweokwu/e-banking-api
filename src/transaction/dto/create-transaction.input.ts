import { InputType, Field, ID, Float } from '@nestjs/graphql';
import { ObjectId } from 'mongoose';

@InputType()
export class CreateTransactionInput {
  @Field(() => ID, { description: 'User ID' })
  userId: ObjectId;

  @Field(() => Float, { description: 'Transaction amount' })
  amount: number;

  @Field({ description: 'Transaction type (e.g., deposit, withdrawal)' })
  type: string;

  @Field(() => String)
  recipientPhoneNo: string;

  @Field(() => Date, { description: 'Timestamp of the transaction' })
  timestamp: Date;
}
