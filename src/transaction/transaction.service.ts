import { Injectable } from '@nestjs/common';
import { CreateTransactionInput } from './dto/create-transaction.input';
import { Model } from 'mongoose';
import {
  Transaction,
  TransactionDocument,
} from './entities/transaction.entity';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class TransactionService {
  constructor(
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
  ) {}

  async create(
    createTransactionInput: CreateTransactionInput,
  ): Promise<Transaction> {
    try {
      const newTransaction = new this.transactionModel(createTransactionInput);
      return await newTransaction.save();
    } catch (error) {
      throw new Error(`Failed to create transaction: ${error.message}`);
    }
  }

  async findTransactionsByUserId(userId: string): Promise<Transaction[]> {
    try {
      return await this.transactionModel
        .find({ userId })
        .sort({ timestamp: -1 });
    } catch (error) {
      throw new Error(
        `Failed to find transactions by user ID: ${error.message}`,
      );
    }
  }
}
