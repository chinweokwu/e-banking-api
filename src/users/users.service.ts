import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema as MongooSchema } from 'mongoose';
import { CreateUserInput } from './dto/create-user.input';
import { User, UserDocument } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private userModel: Model<UserDocument>,
  ) {}

  async create(createUserInput: CreateUserInput) {
    const createdUser = new this.userModel(createUserInput);

    return createdUser.save();
  }

  async findAll(): Promise<User[]> {
    try {
      return await this.userModel.find().exec();
    } catch (error) {
      throw new Error('Failed to fetch users');
    }
  }

  async findOne(username: string): Promise<User> {
    try {
      return await this.userModel.findOne({ username }).exec();
    } catch (error) {
      throw new Error('Failed to find user by username');
    }
  }

  async findByPhoneNo(phoneNo: string): Promise<User> {
    try {
      return await this.userModel.findOne({ phoneNo }).exec();
    } catch (error) {
      throw new Error('Failed to find user by phoneNo');
    }
  }

  async findById(id: MongooSchema.Types.ObjectId): Promise<User> {
    try {
      const user = await this.userModel.findById(id).exec();
      if (!user) {
        throw new NotFoundException('User not found');
      }
      return user;
    } catch (error) {
      throw new Error('Failed to get user by ID');
    }
  }


  async updateBalance(
    id: MongooSchema.Types.ObjectId,
    newBalance: number,
  ): Promise<User | null> {
    try {
      const updatedUser = await this.userModel
        .findByIdAndUpdate(id, { balance: newBalance }, { new: true })
        .exec();

      if (!updatedUser) {
        throw new NotFoundException('User not found');
      }

      return updatedUser;
    } catch (error) {
      throw new Error('Failed to update user balance: ' + error.message);
    }
  }
}
