import { Resolver, Query, Args, Float } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { CurrentUser } from './../auth/user.decorator';

@Resolver(() => User)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(() => [User], { name: 'users' })
  @UseGuards(JwtAuthGuard)
  findAll() {
    return this.usersService.findAll();
  }

  @Query(() => User, { name: 'user' })
  findOne(@Args('username') username: string) {
    return this.usersService.findOne(username);
  }

  @Query(() => User)
  @UseGuards(JwtAuthGuard)
  async me(@CurrentUser() user: any): Promise<User> {
    return this.usersService.findById(user.userId);
  }

  @Query(() => Float, { name: 'myBalance' })
  @UseGuards(JwtAuthGuard)
  async getMyBalance(@CurrentUser() user: any): Promise<number> {
    const updatedUser = await this.usersService.findById(user.userId);
    return updatedUser.balance;
  }
}
