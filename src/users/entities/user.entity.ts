import { ObjectType, Field, Float } from '@nestjs/graphql';
import { Document, Schema as MongooSchema } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';


@ObjectType()
@Schema()
export class User {
  @Field(() => String)
  _id: MongooSchema.Types.ObjectId;

  @Field(() => String)
  @Prop({ required: true, unique: true, minlength: 3, maxlength: 30 })
  username: string;

  @Field(() => String)
  @Prop({ required:true, unique: true, minlength: 11, maxlength: 11 })
  phoneNo: string;

  @Field(() => String)
  @Prop({ required: true})
  password: string;

  @Field(() => Float)
  @Prop({ default: 0 })
  balance: number;
}

export type UserDocument = User & Document;
export const UserSchema = SchemaFactory.createForClass(User);
