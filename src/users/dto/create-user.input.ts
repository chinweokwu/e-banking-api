import { InputType } from '@nestjs/graphql';
import { Prop } from '@nestjs/mongoose';

@InputType()
export class CreateUserInput {
  @Prop({ required: true, unique: true, minlength: 3, maxlength: 30 })
  username: string;

  @Prop({ required:true, unique: true, minlength: 11, maxlength: 11 })
  phoneNo: string;

  @Prop({ required: true})
  password: string;
}
