## Description
This is a simple Nest.js and GraphQL banking application. The application allows users to be able to sign up and sign in while their phone number represents their account number.
Users can also transfer money to other users and deposit/withdraw into/from their account and view their transaction history.


## Tools

- Nest.js
- GraphQL playground (GraphQL)
- Testing Frameworks: Jest (for unit tests)
- Vercel (for deployment)
- Docker (for containerization)
- Docker Compose (for running the app in Docker)
- MongoDB Atlas (for database)
- Passport.js (for authentication)
- Jwt (for authentication)


## Installation

```bash
$ git clone https://gitlab.com/morah-paul/banking-system-api.git
$ cd banking-system-api
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Running Docker

```bash
# unit tests
$ docker-composer up --build
```


## GraphQL Schema
```bash
type LoginResponse {
  access_token: String!
  user: User!
}

input LoginUserInput {
  password: String!
  username: String!
}

type Mutation {
  deposit(amount: Float!): Transaction!
  login(loginUserInput: LoginUserInput!): LoginResponse!
  signup(registerUserInput: RegisterUserInput!): User!
  transfer(amount: Float!, recipientPhoneNo: String!): Transaction!
  withdraw(amount: Float!): Transaction!
}

type Query {
  me: User!
  myBalance: Float!
  transactionHistory: [Transaction!]!
  user(username: String!): User!
  users: [User!]!
}

input RegisterUserInput {
  password: String!
  phoneNo: String!
  username: String!
}

type Transaction {
  amount: Float!
  recipientPhoneNo: String!
  timestamp: DateTime!
  type: String!
  userId: String!
}

type User {
  _id: String!
  balance: Float!
  password: String!
  phoneNo: String!
  username: String!
}
```
## Endpoints
visit http://localhost:3000/graphql to test the API Endpoints
<img src="./assets/img1.png"  alt="graphql" />
<img src="./assets/img2.png"  alt="graphql" />
<img src="./assets/img3.png"  alt="graphql" />
<img src="./assets/img4.png"  alt="graphql" />
<img src="./assets/img5.png"  alt="graphql" />
<img src="./assets/img6.png"  alt="graphql" />


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Gmail - [Morah Paul](paulcmorah@gmail.com)
- GitHub - [https://github.com/chinweokwu](https://github.com/chinweokwu)
- GitLab - [https://gitlab.com/morah-paul](https://gitlab.com/morah-paul)

## License

Nest is [MIT licensed](LICENSE).
