import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import * as fs from 'fs';
jest.setTimeout(70000);

describe('Transactions (E2E)', () => {
  let app: INestApplication;
  let testData: any;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    try {
      testData = JSON.parse(fs.readFileSync('test-data.json', 'utf-8'));
    } catch (error) {
      console.error('Error reading test data:', error.message);
      throw error;
    }
  });

  afterAll(async () => {
    await app.close();
  });

  it('/graphql (POST) - Deposit', async () => {
    const depositData = {
      userId: 'user-2',
      amount: 1000,
    };

    const depositResponse = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: `
          mutation Deposit($amount: Float!) {
            deposit(amount: $amount) {
              amount
              timestamp
              type
              userId
            }
          }
        `,
        variables: depositData,
      });

    expect(depositResponse.status).toBe(200);
    const updatedUser = testData.users.find(
      (user) => user.id === depositData.userId,
    );
    expect(updatedUser.balance).toBeGreaterThan(0);

    updatedUser.balance += depositData.amount;
    testData.transactions.push({
      ...depositData,
    });
    fs.writeFileSync('test-data.json', JSON.stringify(testData, null, 2));
  });

  it('/graphql (POST) - Withdraw', async () => {
    const withdrawalData = {
      userId: 'user-1',
      amount: 50,
    };

    const withdrawalResponse = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: `
          mutation Withdraw($amount: Float!) {
            withdraw(amount: $amount) {
              amount
              timestamp
              type
              userId
            }
          }
        `,
        variables: withdrawalData,
      });

    expect(withdrawalResponse.status).toBe(200);

    const updatedUser = testData.users.find(
      (user) => user.id === withdrawalData.userId,
    );
    expect(updatedUser.balance).toBeGreaterThan(0);

    updatedUser.balance -= withdrawalData.amount;
    testData.transactions.push({
      ...withdrawalData,
    });
    fs.writeFileSync('test-data.json', JSON.stringify(testData, null, 2));
  });

  it('/graphql (POST) - Transaction History', async () => {
    const userId = 'user-2';

    // Perform a request to fetch transaction history
    const historyResponse = await request(app.getHttpServer())
      .post('/graphql') // Use the GraphQL endpoint
      .send({
        query: `
          query TransactionHistory {
            transactionHistory {
              amount
              timestamp
              type
              userId
            }
          }
        `,
        variables: { userId }, // Pass the userId as a variable
      });

    // Verify response and transaction history
    expect(historyResponse.status).toBe(200); // Use 200 for GraphQL queries
  });
});
