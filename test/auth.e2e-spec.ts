import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
jest.setTimeout(70000);

describe('Auth (E2E)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('GraphQL - Signup', async () => {
    const newUser = {
      username: 'testuser2',
      password: 'testpassword2',
      phoneNo: '1234567892',
    };

    const signupResponse = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: `
          mutation Signup($input: RegisterUserInput!) {
            signup(registerUserInput: $input) {
              username
              _id
              phoneNo
              balance
            }
          }
        `,
        variables: { input: newUser },
      });

    expect(signupResponse.status).toBe(200);
  });

  it('GraphQL - Signup (User Already Exists)', async () => {
    const existingUser = {
      username: 'testuser',
      password: 'testpassword',
      phoneNo: '1234567890',
    };

    const signupResponse = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: `
          mutation Signup($input: RegisterUserInput!) {
            signup(registerUserInput: $input) {
              username
              _id
              phoneNo
              balance
            }
          }
        `,
        variables: { input: existingUser },
      });

    expect(signupResponse.body.errors).toBeDefined();
    expect(signupResponse.body.errors[0].message).toBe(
      'Error during signup: User already exists',
    );
  });

  it('GraphQL - Login', async () => {
    const loginData = {
      username: 'testuser',
      password: 'testpassword',
    };

    const loginResponse = await request(app.getHttpServer())
      .post('/graphql')
      .send({
        query: `
          mutation Login($input: LoginUserInput!) {
            login(loginUserInput: $input) {
              user {
                username
                phoneNo
                balance
              }
              access_token
            }
          }
        `,
        variables: { input: loginData },
      });

    expect(loginResponse.status).toBe(200);

    const accessToken = loginResponse.body.data.login.access_token;
    expect(accessToken).toBeDefined();
  });
});
